﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TFGCommonLib.Networking;
using TFGCommonLib.Utils;

namespace GameLauncher {
    public partial class MainWindow : Form {

        private const string gameClientName = "GameClient.exe";
        private const string configFileName = "LauncherConfig.ini";
        private const string gameClientFolder = "GameClient";

        private string serverIP = "192.168.1.10";
        private int serverPort = 1994;
        private int versionMajor = 0;
        private int versionMinor = 0;
        private int versionRevision = 0;

        private string[] serverVersion;

        private static bool downloading = false;
        private Thread downloadThread;

        INIFile iniFile = new INIFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + configFileName);

        delegate void SetIntCallback(int value);
        delegate void SetTextBoolCallback(string text, bool boolean);
        delegate void SetCallback();

        public MainWindow() {
            InitializeComponent();

            if (hasConfig())
                readConfigFile();
            else
                createConfigFile();

            FormClosing += MainWindow_FormClosing;
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e) {
            if (downloading) {
                //Delete new download folder and files, and rename old to current
                if (Directory.Exists(gameClientFolder)) {
                    DirectoryInfo di = new DirectoryInfo(gameClientFolder);
                    foreach (FileInfo file in di.GetFiles())
                        file.Delete();
                    foreach (DirectoryInfo dir in di.GetDirectories())
                        dir.Delete(true);
                    Directory.Delete(gameClientFolder);
                }

                Directory.Move(new StringBuilder().Append(gameClientFolder).Append(".old").ToString(), gameClientFolder);
                ConnectionManager.Instance.Disconnect();
            }
            else {
                ConnectionManager.Instance.Disconnect();
            }
        }

        private void MainWindow_Load(object sender, EventArgs e) {
            checkForUpdate();
        }

        private void playButton_Click(object sender, EventArgs e) {
            if (playButton.Text.Equals("UPDATE")) {
                setEmptyDownloadBar();
                setButtonPlay("UPDATING", false);

                downloadThread = new Thread(downloadGame);
                downloadThread.Start();

            }
            else if (playButton.Text.Equals("DOWNLOAD")) {
                setEmptyDownloadBar();
                setButtonPlay("DOWNLOADING", false);

                downloadThread = new Thread(downloadGame);
                downloadThread.Start();

            }
            else if (playButton.Text.Equals("PLAY")) {
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + "\\" + gameClientFolder + "\\" + gameClientName);
                Application.Exit();
            }
            else {
                setEmptyDownloadBar();
                setButtonPlay("CHECKING FOR UPDATES", false);
                checkForUpdate();
            }
        }

        private bool hasConfig() {
            return File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\" + configFileName);
        }

        private void createConfigFile() {
            iniFile.Write("Server", "ip", serverIP);
            iniFile.Write("Server", "port", serverPort.ToString());
            iniFile.Write("Version", "major", versionMajor.ToString());
            iniFile.Write("Version", "minor", versionMinor.ToString());
            iniFile.Write("Version", "revision", versionRevision.ToString());
        }

        private void readConfigFile() {
            serverIP = iniFile.Read("Server", "ip");
            serverPort = int.Parse(iniFile.Read("Server", "port"));
            versionMajor = int.Parse(iniFile.Read("Version", "major"));
            versionMinor = int.Parse(iniFile.Read("Version", "minor"));
            versionRevision = int.Parse(iniFile.Read("Version", "revision"));

            //serverIP = "192.168.1.13";
        }

        private bool isClientDownloaded() {
            return File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\" + gameClientFolder + "\\" + gameClientName);
        }

        private void checkForUpdate() {
            try {
                ConnectionManager.Instance.Connect(serverIP, serverPort);
                ConnectionManager.Instance.Send(OpCode.GET_VERSION);
                string message = ConnectionManager.Instance.Receive();

                serverVersion = MessageHandler.ReadParams(message);

                if (!isClientDownloaded()) {
                    setButtonPlay("DOWNLOAD", true);
                    setEmptyDownloadBar();
                }
                else {
                    if (int.Parse(serverVersion[0]) == versionMajor &&
                    int.Parse(serverVersion[1]) == versionMinor &&
                    int.Parse(serverVersion[2]) == versionRevision) {
                        playButton.Text = "PLAY";
                        setButtonPlay("PLAY", true);
                        setFullDownloadBar();
                    }
                    else {
                        setButtonPlay("UPDATE", true);
                        setEmptyDownloadBar();
                    }
                }
                ConnectionManager.Instance.Disconnect();
            }
            catch (Exception ex) {
                setButtonPlay("CHECK FOR UPDATES", true);
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        private void downloadGame() {
            ConnectionManager.Instance.Connect(serverIP, serverPort);
            ConnectionManager.Instance.Send(OpCode.DOWNLOAD_CLIENT);

            string message = ConnectionManager.Instance.Receive();
            int transferData = int.Parse(MessageHandler.ReadParams(message)[0]);

            setDownloadBarMaximum(transferData);

            if (!Directory.Exists(gameClientFolder)) {
                Directory.CreateDirectory(gameClientFolder);
            }
            else {
                Directory.Move(gameClientFolder, new StringBuilder().Append(gameClientFolder).Append(".old").ToString());
                Directory.CreateDirectory(gameClientFolder);
            }

            ConnectionManager.Instance.Send(OpCode.TRANSFER_PREPARED);
            downloading = true;

            int received = 0;
            while (received < transferData) {
                message = ConnectionManager.Instance.Receive();

                try {
                    string[] fileParams = MessageHandler.ReadParams(message);

                    string file = fileParams[0].Replace("\\", "/").Replace('*', ' ');
                    int size = int.Parse(fileParams[1]);

                    string[] folders = file.Split('/');
                    string path = "";
                    for (int i = 0; i < folders.Length - 1; i++) {
                        path += folders[i] + "/";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                    }

                    ConnectionManager.Instance.Send(OpCode.TRANSFER_PREPARED);

                    FileStream fs = File.Create(file, 1024);

                    byte[] buffer;
                    while (fs.Length < size) {
                        if (size - fs.Length >= 1024)
                            buffer = new byte[1024];
                        else
                            buffer = new byte[size - fs.Length];

                        int bytesRead = ConnectionManager.Instance.ClientSocket.Receive(buffer, buffer.Length, System.Net.Sockets.SocketFlags.None);

                        //Console.WriteLine(bytesRead);

                        byte[] realBuffer = new byte[bytesRead];
                        for (int i = 0; i < bytesRead; i++) {
                            realBuffer[i] = buffer[i];
                        }

                        fs.Write(realBuffer, 0, bytesRead);

                    }

                    fs.Close();

                    increaseDownloadBarValue();
                    received++;

                    ConnectionManager.Instance.Send(OpCode.TRANSFER_FINISHED);
                }
                catch (Exception e) {
                    Console.WriteLine(e.StackTrace.ToString());
                    return;
                }
            }
            downloading = false;

            //Delete .old folder and files
            if (Directory.Exists(gameClientFolder + ".old")) {
                DirectoryInfo di = new DirectoryInfo(gameClientFolder + ".old\\");
                foreach (FileInfo file in di.GetFiles())
                    file.Delete();
                foreach (DirectoryInfo dir in di.GetDirectories())
                    dir.Delete(true);
                Directory.Delete(gameClientFolder + ".old");
            }

            if (!hasConfig())
                createConfigFile();
            iniFile.Write("Version", "major", serverVersion[0]);
            iniFile.Write("Version", "minor", serverVersion[1]);
            iniFile.Write("Version", "revision", serverVersion[2]);

            setButtonPlay("PLAY", true);
            ConnectionManager.Instance.Disconnect();
        }

        //FORMS METHODS

        private void setDownloadBarMaximum(int value) {
            if (downloadBar.InvokeRequired) {
                SetIntCallback d = new SetIntCallback(setDownloadBarMaximum);
                Invoke(d, value);
            }
            else {
                downloadBar.Maximum = value;
            }
        }

        private void setFullDownloadBar() {
            downloadBar.Maximum = 1;
            downloadBar.Value = 1;
        }

        private void setEmptyDownloadBar() {
            downloadBar.Maximum = 1;
            downloadBar.Value = 0;
        }

        private void increaseDownloadBarValue() {
            if (downloadBar.InvokeRequired) {
                SetCallback d = new SetCallback(increaseDownloadBarValue);
                Invoke(d);
            }
            else {
                downloadBar.Value++;
            }
        }

        private void setButtonPlay(string text, bool enabled) {
            if (playButton.InvokeRequired) {
                SetTextBoolCallback d = new SetTextBoolCallback(setButtonPlay);
                Invoke(d, text, enabled);
            }
            else {
                playButton.Text = text;
                playButton.Enabled = enabled;
            }
        }
    }
}
