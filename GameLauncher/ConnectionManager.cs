﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TFGCommonLib.Networking;

namespace GameLauncher {
    class ConnectionManager : ConnectionProcessor {

        private static ConnectionManager instance;
        public static ConnectionManager Instance {
            get {
                if (instance == null)
                    instance = new ConnectionManager();
                return instance;
            }
        }

        private Socket clientSocket;
        public Socket ClientSocket {
            get {
                return clientSocket;
            }
        }

        public void Connect(string serverIP, int port) {
            IPAddress ipAddress = IPAddress.Parse(serverIP);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            if (clientSocket == null) {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            else {
                if (!clientSocket.Connected)
                    clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }

            clientSocket.Connect(remoteEP);
        }

        public void Disconnect() {
            if (clientSocket != null && clientSocket.Connected) {
                Send(OpCode.DISCONNECT);
                clientSocket.Shutdown(SocketShutdown.Both);
                clientSocket.Close();
            }
        }

        public override void Send(OpCode opCode, params string[] args) {
            send(MessageHandler.CreateMessage(opCode, args));
        }

        public override void Send(ErrorCode errorCode, params string[] args) {
            send(MessageHandler.CreateMessage(errorCode, args));
        }

        protected override void send(string message) {
            byte[] sendBytes = Encoding.ASCII.GetBytes(message);

            if (clientSocket.Connected) {
                clientSocket.Send(sendBytes);
            }
        }

        public override string Receive() {
            byte[] bytesFrom = new byte[1024];
            string message = null;

            if (clientSocket.Connected) {

                clientSocket.Receive(bytesFrom);

                message = Encoding.ASCII.GetString(bytesFrom).TrimEnd((char)0);
            }

            return message;
        }

        protected override void receive() {
            throw new NotImplementedException();
        }
    }
}
